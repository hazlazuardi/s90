// Main


// TO CODE

// URLs
// APIs
const GoogleAPI_URL = 'https://www.googleapis.com/books/v1/volumes?q=';
// My Site
// const myURL = 'http://127.0.0.1:8000/';
const myURL = 'story9haz.herokuapp.com'

// Global Event for Search System
document.getElementById("searchForm").addEventListener("submit", searchQ);

// Global Event for Rank System
// Rank Button Event
$("#rankBtn").on("click", function() {
    rankQ();
});
// Modal Button Event
$(".modal").on("click", function() {
    if ($(this).css("display", "flex")) {
        $(this).css("display", "none");
    }
})



// Functions for Search System
// Function mainly for searching and give APIs q=
function searchQ(e) {
    // Get input value from input:search (HTML)
    let input = document.getElementById("searchText");
    console.log(input.value);
    // Final URL with q= to request to APIs
    const resultURL = GoogleAPI_URL + input.value;
    console.log(resultURL);
    // Start AJAX 'GET'
    $.ajax({
        type: 'GET',
        url: resultURL,
        success: function(data) {
            // Run func to show the result
            loadData(data);
            // Run func if user like the element
            likeData();
            console.table([data.items], ['volumeInfo']);
        },
        error: function() {
            alert('Error cok');
        }
    });
    e.preventDefault();
}

// Function to show & put the search result
function loadData(data) {
    // get the parent for putting the rendered result
    let parent = document.getElementById('searchResult');
    // empty the parent's child
    parent.innerHTML = '';
    // The JSON contains many items, loop it to get eachData
    for (i = 0; i < data.items.length; i++) {
        // For eachData, render each of them and put inside parent.
        parent.insertAdjacentHTML('beforeend', renderBookList(data.items[i]));
    }
}

// Function to render HTML of the search result
function renderBookList(data) {
    // Destructuring for concise var, get each keys
    var { infoLink, imageLinks, title, infoLink } = data.volumeInfo;
    // Get the element's unique id
    var id = data.id;
    console.table([
        `id buku ${title}: ${id}`,
        `title buku ${title}: ${title}`,
        `info buku ${title}: ${infoLink}`,

    ]);
    // return the HTML
    // add class book for Functional Testing
    return (
        `<div class="book" id="${id}~${title}~${imageLinks ? imageLinks.thumbnail : "noImg"}~${infoLink}">
            <div>
                <a href="${infoLink}" target='blank'>
                    <img src="${(imageLinks ? imageLinks.thumbnail : null )}" alt=''>
                </a>
                <div class="like">
                    <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                        <path d="M0 0h24v24H0V0z" fill="none"/>
                        <path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-2z"/>
                    </svg>                
                </div>
                <h3>${title}</h3>
            </div>
        </div>`
    )

}


// Functions for Rank System

// Function mainly for showing the rank system
function rankQ() {
    // Start AJAX 'POST' to get data from my database
    $.ajax({
        type: "POST",
        url: "",
        success: function(res) {
            // parse the data to JSON.
            var books = JSON.parse(res);
            console.table([books]);
            // get the parent to put our rank data
            let parent = document.getElementById('rankResult');
            // empty the parent
            parent.innerHTML = '';
            // Use loop to access eachData
            for (var i = books.length - 1; i >= 0; i--) {
                // Destructuring to make it concise.
                const { title, likes, image_url } = books[i];
                console.log(`${title}\n${likes}`);
                // Render eachData to show & put inside the parent
                parent.insertAdjacentHTML("beforeend", renderRanking(books[i])

                )
            };
        }

    });
    // When the function runs, change the display style of .modal to show it (Before display:none)
    $(".modal").css('display', 'flex');
}

// Function when user like an element
function likeData() {
    // Get the event when user click like button
    $('.like').on('click', function() {
        // get its children (SVG) and change the fill to blue
        $(this).children().attr('fill', "#2256CC");
        // Get the data after passing it to the id attr
        var data = $(this).parent().parent().attr('id').split("~");
        console.table(data)
            // Start AJAX 'POST' to send to our database.
        $.ajax({
            type: "POST",
            url: "/like/",
            dataType: "json",
            data: {
                // Each fills the models.py
                'id': data[0],
                'title': data[1],
                'img': data[2],
                'info': data[3],
            },
            success: function(e) {
                // User feedback when liking an element
                alert(`You liked ${data[1]}! Total Like = ${e}`);
            }
        })
    })
}

// Function to render HTML for Rank System
function renderRanking(eachData) {
    // Destructuring to make it concise
    const { title, image_url, likes, info_url } = eachData;
    console.table([eachData])
        // Return the HTML
    return (
        `<div>
        <a href="${info_url}">
            <img src="${image_url}">
        </a>
        <p>Total Likes: ${likes}</p>
        <h4>${title}</h4>
        </div>`
    )
}