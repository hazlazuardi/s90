from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.staticfiles import finders

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

from .views import *
from .models import *

heroku = 'http://story9haz.herokuapp.com/'
# local = 'http://127.0.0.1:8000/'
local = heroku 

# Create your tests here.
class S90UnitTest(TestCase):

    def test_S90_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_S90_index_func_is_used(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_S90_index_func_post_is_used(self):
        response = Client().post('/', {
            'id':'s0m3r4nd0mstvff', 
            'title':"How to not to", 
            'img':'httpduarjuga', 
            'info':'httpduar'
            })
        self.assertEqual(response.status_code, 200)
    
    def test_S90_url_like_is_exist(self):
        response = Client().post('/like/', {
            'id':'s0m3r4nd0mstvff', 
            'title':"How to not to", 
            'img':'httpduarjuga', 
            'info':'httpduar'
            })
        self.assertEqual(response.status_code, 200)

    def test_S90_like_func_is_used(self):
        found = resolve('/like/')
        self.assertEqual(found.func, like)

    def test_S90_model_Book(self):
        Book.objects.create(
            title="How to not to", 
            likes=1000
            )
        self.assertEqual(Book.objects.all().count(), 1)

    def test_S90_model_increment_like(self):
        Book.objects.create(
            title="How to not to", 
            likes=1000
            )
        book = Book.objects.get(title="How to not to")
        book.likes+=1
        book.save()
        self.assertEqual(Book.objects.get(
            title="How to not to").likes, 1001
            )
    def test_S90_template_index_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_S90_static_css_is_exist(self):
        response = finders.find('css/main.css')
        self.assertNotEqual(response, None)

    def test_S90_static_js_is_exist(self):
        response = finders.find('js/main.js')
        self.assertNotEqual(response, None)
    

class S90FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(S90FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(S90FunctionalTest, self).tearDown()

    def test_S90_url(self):
        selenium = self.selenium
        selenium.get(local)

    def test_S90_search_then_load_and_display_result(self):
        selenium = self.selenium
        selenium.get(local)
        input = selenium.find_element_by_id('searchText')
        input.send_keys("dada")
        input.send_keys(Keys.RETURN)
        selenium.implicitly_wait(1.5)
        book = selenium.find_element_by_css_selector('.book')

    def test_S90_search_using_button_then_load_and_display_result_then_like_Dada_and_Surrealism_20_times(self):
        selenium = self.selenium
        selenium.get(local)
        input = selenium.find_element_by_id('searchText')
        input.send_keys("W3kRDAAAQBAJ")
        input.send_keys(Keys.RETURN)
        selenium.implicitly_wait(1.5)
        book = selenium.find_element_by_css_selector('.book')
        like = selenium.find_element_by_css_selector('.like')
        try:
            alert_obj = self.driver.switch_to_alert()
        except: 
            return False        
        for i in range(20):
            time.sleep(.2)
            selenium.execute_script("arguments[0].click();", like)
            alert_obj.accept()

    def test_S90_get_top_liked_books(self):
        selenium = self.selenium
        selenium.get(local)
        top = selenium.find_element_by_id('rankBtn')
        top.click()
        selenium.implicitly_wait(1.5)
        modal = selenium.find_element_by_css_selector('.modal')
        modal.click()






