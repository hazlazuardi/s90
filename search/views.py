from django.shortcuts import render, HttpResponseRedirect
from django.http import HttpResponse
from .models import Book
from django.views.decorators.csrf import csrf_exempt
import json


# Create your views here.

@csrf_exempt
def index(request):
    # If user wanna see the ranking
    if request.method == "POST":
        books = list(Book.objects.values()[:5])
        return HttpResponse(json.dumps(books))
    # If user just wanna see the page
    else:
        return render(request, "landing.html")

@csrf_exempt
def like(request):
    if request.method == 'POST':
        id = request.POST['id']
        title = request.POST['title']
        img = request.POST['img']
        info = request.POST['info']
        book, created = Book.objects.get_or_create(
            id=id, 
            title=title, 
            image_url=img,
            info_url=info
            )
        book.likes += 1
        book.save()
        return HttpResponse(book.likes)
