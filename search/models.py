from django.db import models

# Create your models here.
class Book(models.Model):
    id = models.CharField(max_length=200, primary_key=True ,unique=True)
    title = models.CharField(max_length=256)
    image_url = models.CharField(max_length=269)
    likes = models.IntegerField(default=0)
    info_url = models.CharField(max_length=269)


    class Meta:
        ordering = ('-likes',)

    def __str__(self):
        return self.title
    objects = models.Manager()